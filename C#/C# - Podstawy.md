## Podstawy języka C#

### Zmienne oraz interakcja z konsolą
#### Zmienne
W języku C# występuje silne typowanie co oznacza,że musimy podać typ zmiennej przed przypisanem wartości.
Język C# posiada podstawowe typy takie jak
* int(liczba całkowite)
* double(liczby rzeczywiste podwójnej precyzji)
* float(liczba rzeczywiste)
* bool(boolean)
* char(zmienne znakowe)
* string(zmienna tekstowa)
```C#
int liczba = 0;
bool prawda = true;
string tekst = "tekst";
double liczba_r = 0.0;
```

Istnieje wyjątek od tej reguły jeśli wykorzystamy słowo kluczowe **var**. Po zastosowaniu tego słowa kluczowego typ zmiennej jest wydobywany z podanej wartości.
```C#
var liczba = 0.0 // Typ tej zmiennej to double
```
#### Interakcja z konsolą
By wypisać pewną wartośc na ekranie używamy funkcji następujących instrukcji
```C#
Console.WriteLine(); // Wypisuje całej zawartości i zakończone jest znakiem nowej linii
Console.Write(); // Wypisuje tylko dany znak i wypisanie jest zakończone pustem znakiem
```

By odczytać dane od użytkownika używamy instrukcji **Console.ReadLine()**
```C#
string imie = Console.ReadLine();
```

#### Rzutowanie zmiennych na inny typ
By wykorzystać wartość zmiennej lub zwracaną wartość,ale w innej postaci pod innym typem używamy rzutowania,które możemy przeprowadzić na dwa sposoby.Pierwszy to poprzez wykorzystanie odpowiednich metod specyficznych dla danych typów zmiennych.
```C#
int liczba = Convert.ToInt32(Console.ReadLine()); // Rzutowanie zmiennej tekstowej na typ int
```

Drugi sposób to rzutowanie poprzez umieszczenie przed wartością,którą chcemy rzutować w nawiasach typ,na który chcemy rzutować.
```C#
int liczba = (int) 3.4;
```

#### Stałe
By zadeklować stałą wartość czyli taką,której wartości nie będziemy mogli zmieniać używamy słowa kluczowego **const**
```C#
const double PI  = 3.14;
```

### Instrukcje warunkowe
#### Instrukcja warunkowa IF ELSE IF ELSE
By sprawdzić pewien warunek korzystamy z instrukcji **if** lub w celu podania kolejnych warunków to **else if**. Gdy jest tylko jedna operacja to nie musimy umieszczać nawiasów klamrowych.
```C#
if(warunek1){
    operacja
} else if(warunek 2){
    operacja
} else{
    operacja
}
```

#### Instrukcja switch
Kolejną z instrukcji,która pozwala nam sprawdzać warunki,a dokładnie sterować działaniem programu za pomocą pewnych wartości jest instrukcja **switch** . W instrukcji switch podajemy zmienną,według której będziemy sterować dalszym działaniem programu na podstawie jej wartości.Kolejne przypadki podajemy w postaci **case'ów**,gdzie po każdym umieszczamy instrukcje **break**,która zatrzymuje nam działanie instrukcji switch i wychodzi poza jej zasięg i powoduje dalsze działanie programu.Gdy nie podamy w pewnym miejscu instrukcji **break** to kolejne instrukcję będą wykonywać się do momentu wyjścia z switcha lub wywołania akcji domyślnej. W przypadku,gdy zmienna,którą podamy nie będzie zawierać kolejnych zdefinowanych przez nas przypadków,w tym momencie wykona się akcja domyślna oznaczona przez słowo kluczowe **default**.

```C#
int liczba = 3;
switch(liczba){
    case 1:{
        operacja
        break;
    }
    case 3:{
        operacja
        break;
    }
    default:{
        operacja
    }
}
```

### Pętle
#### Pętla for
Pętla for jest rodzajem pętli,gdzie podajemy wartośc początkową,podajemy warunek końcowy dla osiągniętej wartości oraz podajemy krok,który definiuje zmianę wartości kontrolnej podczas każdej iteracji.
Pętla for ma następującą budowę
```C#
for(int i=0; i<3; i++){
    Console.WriteLine(i);
}
```
Powyższy przykład da efekt wypisania liczb od 0 do 2,gdzie kaza będzie w nowej linii;

#### Pętla While
Pętla While polega na zdefinowaniu warunku i wewnątrz pętli kontrolowanie przebiegu kolejnej iteracji. To znaczy,że możemy sobie ustawić zmienna na zewnątrz i podczas każdej iteracji zmieniać jej wartość w celu kontrolowania działań.
Pętla While ma następującą budowę
```C#
int liczba = 3;
while(liczba>0){
    operacja 
    liczba--;
}
```
W powyższym przykładzie mamy podany warunek i wewnątrz pętli zmieniamy wartość zmiennej kontrolnej,która decyduje o tym czy pętla się wykona kolejny raz czy nie.

#### Pętla Do While
Pętla Do While jest podobna do pętli While,tylko posiada jeden dodatkowy szczegół.Zawsze wykonuje się przynajmniej jeden raz,a wynika to z budowy samej pętli.
```C#
int liczba = 3;
do{
    operacje
    liczba--;
} while(liczba >3)
```
Najpierw mamy blok **do**,który się wykona,a potem następuje sprawdzenie warunku,decydującego o kolejnym wykonaniu się operacji w bloku **do**

### Tablice
#### Tablice jednowymiarowe
Do przetrzymywania większej ilości danych za pomocą jednej zmiennej wykorzystujemy tablice,które mogą posiadać wiele wymiarów. Na razie skupmy się na tablicach jednowymiarowych. Tablicę jednowymiarową możemy wyobrazić sobie jako pasek złożony z kolejnych klocków domino.Każdy klocek reprezenetuje pewną wartość i posiada swoje miejsce. By odwołać się danej wartości używamy jego numeru porządkowego czyli indeksu(indeksujemy od 0).Stworzyć tablicę możemy na dwa sposoby:
* bez podawania wartości,podając tylko maksymalną ilośc danych
```C#
double[] liczby = new double[3];
```
Na powyszym przykładzie stworzyliśmy tablicę liczby typu double(liczby rzeczywiste),gdzie określiliśmy jej pojemność.

* wraz z podaniem elementów,gdzie nie określamy maksymalnej ilości elementów bezpośrednio
```C#
double[] liczby = {1,2.3,4.1};
```
Na powyższym przykładzie widać,że stworzyliśmy podobną tablicę co do poprzedniej tylko tym razem podaliśmy w nawiasach klamrowych(wąsach) gotowe elementy.Ilośc elementów podana podczas tej deklaracji określa maksymalną ilość elementów.
By sprawdzić długość tablicy używamy własności **Length()**.
```C#
double[] liczby = {1,2.3,4.1};
liczby.Length // 3
```

By odwołać się do konkretnej wartości podajemy nazwę naszej tablicy i w kwadratowych nawiasach podajemy odpowiedni indeks.
```C#
double[] liczby = {1,2.3,4.1};
liczby[2] //4.1
```

#### Tablice wielowymiarowe
Spróbujmy rozpatrzeć przykład,gdzie w tablicy jednowymiarowej chcemy przetrzymywać dane,które mają reprezentować nam macierz lub tabelę. Za pomocą tabeli jednowymiarowej będzie ciężko,ponieważ będzie trzeba uważnie dobierać indeksy by odczytać odpowiednie wartości.W takim celu powstały tablice wielowymiarowe.Ich definicja nie różni się zbytnio od podstawowych jednowymiarowych tablic.
```C#
// Tworzenie za pomocą określania ilości elementów
double[,] macierz = new double[3,3] // Tablica dwuwymiarowa
int[, ,] tablica3d = new int[4, 2, 3]; // Tablica trójwymiarowa

// Tworzenie tablic za pomocą podaniu elementów
int[,] tablica2d  = { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } };
int[, ,] tablica3d = new int[,,] { { { 1, 2, 3 }, { 4, 5, 6 } , { { 7, 8, 9 }, { 10, 11, 12 } } };
//Tworzenie tablic za pomocą połączenia dwóch powyższych sposobów 
int[,] tablica2d = new int[4, 2] { { 1, 2 }, { 3, 4 }, { 5,6, { 7, 8 } };
int[, ,] array3Da = new int[2, 2, 3] { { { 1, 2, 3 }, { 4, 5, 6 } }, { { 7, 8, 9 }, { 10, 11, 12 } } };

```

Odwoływanie do poszczególnych elementów odbywa się w podobny sposób indeks do każdego wymiaru podajemy po przecinku
```C#
int[,] tablica2d  = { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } };

tablica2d[0,1] // 2
```
Co do długośc poszczególnego wymiaru to używamy metody **GetLength()**,gdzie jako argument podajemy wymiar.
```C#
int[,] tablica2d  = { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } };

tablica2d.GetLength(0); // 4

```


#### Pętla For Each

By operować na każdym elemencie tablicy możemy wykorzystać specjalną pętlę,która pobiera każdy element z tablicy i nie musimy definiować żadnych warunków zakończenia pętli.Sama pętla przechodzi tyle iteracji ile elementów ma podany zbiór.
```C#
int[] tablica = {1,2,3,4};
foreach(var x in tablica){
    Console.WriteLine(x);
}
```
Powyższy przykład wypisze każdy element z zadeklarowanej tablicy w nowej linii.


### Klasy
Klasy zostały stworzone po to by można rozserzyć podstatowe typy oraz móc odzwierciedlić rzeczywistość.Klasy posiadają swoje pola czyli zmienne przechowujące wartości oraz metody czyli funkcje.
Każda z tych rzeczy jak i sama funkcja posiada okreslenie dostępność dla innych. Ta właśność może mieć wartość :
* public - publicza dla wszystkich
* protected - dostęp dla klas dziedzących
* private - brak dostępu dla wszystkich

Przykład stworzonej klasy
```C#
public class Pies{
    public imie; // Pole klasy
    // Konstruktor

    public Pies(string imie){ 
        this.imie = imie;
    }
    //Metoda 
    public void dajGlos(){
        Console.WriteLine("Hau Hau.Jestem"+imie);
    }
}
```
By stworzyć obiekt takiej klasy postępujemy jak przy tworzeniu zwykłej zmiennej tylko zamiast wartości używamy słowa kluczowego **new** oraz nazwy klasy wraz z nawiasami,gdzie podajemy wartości.
```C#
Pies p = new Pies("Marek");
p.imie = "Mirek"; // Odwołanie się do pól klasy dla danego obiektu.
```

#### Generowanie liczb losowych

By generować liczby losowe w C# trzeba stworzyć do tego odpowiedni obiekt klasy Random
```C#
Random r = new Random();
```

By wygenerować liczbę całkowitą z przedziału od 0 do n używamy funkcji **Next(n)**
```C#
Random r = new Random();
r.Next(n);
```
By wygenerować liczbę całkowitą z przedziału od n do m używamy funkcji **Next(n,m)**
```C#
Random r = new Random();
r.Next(n,m); //r.Next(10,12) zwróci liczbę całkowitą z przedziału od 10 do 12
```
By wygenerować liczbę rzeczywstią z przedziału od 0 do 1 używamy funkcji **NextDouble()**
```C#
Random r = new Random();
r.NextDouble(); 
```
By wygenerować liczbę całkowitą z przedziału od min do max używamy funkcji **NextDouble()*(max - min) + min**
```C#
Random r = new Random();
r.NextDouble()*(10 - 1) + 1; // zwróci liczbę rzeczywstią z przedziału od 1 do 10
```
