﻿using System;
namespace MatrixUtils
{
    public class Matrix
    {
        private int rowsCount;
        private int columnsCount;
        private double[,] matrix;
        public Matrix(int rows,int cols){
            this.rowsCount = rows;
            this.columnsCount = cols;
            matrix = new double[rows,cols];
            
        }
        public int getRowsNumber(){
            return this.rowsCount;
        }
        public int getColumnsNumber(){
            return this.columnsCount;
        }
        private void initMatrix(){
            for(int i=0; i<rowsCount; i++){
                for(int j=0; j<columnsCount; j++){
                    this.matrix[i,j] = 0;
                }
            }
        }
        public static Matrix fromArray(double[,] arr){

            Matrix c = new Matrix(arr.GetLength(0),arr.GetLength(1));
            for(int i=0; i<c.getRowsNumber(); i++){
                for(int j=0; j<c.getColumnsNumber(); j++){
                    c.matrix[i,j] = arr[i,j];
                }
            }
            return c;
        }
        public static double[,] toArray(Matrix m){
            double[,] arr = new double[m.getRowsNumber(),m.getColumnsNumber()];
            for(int i=0; i<m.getRowsNumber(); i++){
                for(int j=0; j<m.getColumnsNumber(); j++ ){
                    arr[i,j] = m.matrix[i,j];
                }
            }
            return arr;
        }
        
          public void printMatrix(){
            for(int i=0; i<rowsCount; i++){
                for(int j=0; j<columnsCount; j++){
                    Console.Write("|{0}|",this.matrix[i,j]);
                }
                Console.WriteLine();

            }
            
        }
        public void add(double n){
            for(int i=0; i<this.rowsCount; i++){
                for(int j=0; j<this.columnsCount; j++){
                    matrix[i,j]+=n;
                }
            }
        }
        public void add(Matrix m){
            if(m.getRowsNumber() == rowsCount && m.getColumnsNumber() == columnsCount){
                for(int i=0; i<rowsCount; i++){
                    for(int j=0; j<columnsCount; j++){
                        matrix[i,j]+=m.matrix[i,j];
                    }
                }
            }
        }
        public static Matrix transpose(Matrix m){
            Matrix m1 = new Matrix(m.getColumnsNumber(),m.getRowsNumber());
            for(int i=0; i<m1.getRowsNumber(); i++){
                for(int j=0; j<m1.getColumnsNumber(); j++){
                    m1.matrix[i,j] = m.matrix[j,i];
                }
            }

            return m1;
        }
        public void multiply(double n){
            for(int i=0; i<rowsCount; i++){
                for(int j=0; j<columnsCount; j++){
                    this.matrix[i,j]*=n;
                }
            }
        }
        public static Matrix multiply(Matrix m1,Matrix m2){
           
                Matrix m3 = new Matrix(m1.getRowsNumber(),m2.getColumnsNumber());
                for(int i=0; i<m3.getRowsNumber(); i++){
                    for(int j=0; j<m3.getColumnsNumber(); j++){
                        double sum = 0;
                        for(int k = 0; k<m1.getColumnsNumber(); k++){
                            sum+= m1.matrix[i,k] * m2.matrix[k,j];
                        }
                        m3.matrix[i,j] = sum;
                    }
                }
                return m3;
            
        }
    }
}
