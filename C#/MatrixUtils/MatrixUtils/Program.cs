﻿using System;

namespace MatrixUtils
{
	class Program
	{
		public static void Main(string[] args)
		{
			double[,] a = { { 2, 1, 3 }, { -1, 4, 0 } };
			double[,] b = { { 1, 3, 2 }, { -2, 0, 1 }, { 5, -3, 2 } };
			Matrix m1 = Matrix.fromArray(a);
			Matrix m2 = Matrix.fromArray(b);

			Console.WriteLine("Przed ");
			Console.WriteLine("Matrix 1");
			m1.printMatrix();
			Console.WriteLine("Matrix 2");
			m2.printMatrix();
			Console.WriteLine("Po");
			Matrix m3 = Matrix.multiply(m1, m2);
			m3.printMatrix();
			Console.ReadKey(true);
		}
	}
}